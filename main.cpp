/***************************************************************************
**
** This file is part of qterminalmodeclient **
**
** Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).*
**
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
**  * Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**  * Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with ** the distribution.
**  * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the
**    names of its contributors may be used to endorse or promote products
**    derived from this software without specific ** prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
**************************************************************************/

#include <QtGui/QApplication>
#include <QTextStream>
#include "QString"

#include "mainapplication.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if (QApplication::arguments().contains("-h") || QApplication::arguments().contains("--help")) {
        QTextStream cout(stdout);
        cout << "\nUsage: qterminalmode [OPTION]...\n";
        cout << "QTerminalMode Client Reference Implementation.\n\n";
        cout << "      --no-scaling \t\t\t disable client side scaling support; fix size of remoted framebuffer\n";
        cout << "      --size <width> <height> \t\t set minimum size for remoted framebuffer\n";
        cout << "      --display <width> <height> \t\t set minimum display size for the head-unit screen\n";
        cout << "      --no-inc \t\t\t\t disable incremental framebuffer updates\n";
        cout << "      --no-rle \t\t\t\t disable run length encoding\n";
        cout << "      --filter \t\t\t\t enable filtering\n";
        cout << "      --ipv6   \t\t\t\t run on IPv6 interfaces (otherwise by default IPv4 interfaces are used)\n";
        cout << "  -h, --help \t\t\t\t display this help message\n\n";
        cout << " color settings \t\t\t (default is RGB888):\n";
        cout << "      --rgb565 \t\t\t\t RGB565\n";
        cout << "      --rgb555 \t\t\t\t RGB555\n";
        cout << "      --rgb444 \t\t\t\t RGB444\n";
        cout << "      --rgb343 \t\t\t\t RGB343\n\n";
        cout << "Report bugs to johannes.zellner@nokia.com and jorg.brakensiek@nokia.com\n\n";
        return 0;
    }

    MainApplication w;
    w.show();

    return a.exec();
}
