/***************************************************************************
**
** This file is part of qterminalmodeclient **
**
** Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).*
**
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
**  * Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**  * Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with ** the distribution.
**  * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the
**    names of its contributors may be used to endorse or promote products
**    derived from this software without specific ** prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
**************************************************************************/

#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>
#include <QLayout>
#include <QHash>
#include <QMultiHash>
#include <QTabWidget>
#include <QtNetwork/QHostAddress>

#include "QTmGlobal.h"
#include "QAppScreen.h"
#include "QTmClient.h"
#include "QTmClientProfile.h"
#include "keyboard.h"

class QTmClient;


class MainApplication : public QWidget
{
    Q_OBJECT
public:
    explicit MainApplication(QWidget *parent = 0);

public slots:
    void clickRed();
    void clickGreen();
    void clickApp();
    void clickAppExit();
    void clickUpnpView();
    void launchApplication(QTmApplication *application);

private slots:
    void deviceDetected(QString interfaceName, QHostAddress interface);
    void deviceLost(QHostAddress interface);

    void vncResized(QSize size, int id);
    void vncClientContextInformation(QRect area, int appId, QTmAppInfo *appCategory, QTmDisplayInfo *conCategory, int id);
    void vncDisconnected(int id);
    void vncConnected(int id);

    void updateApplicationDiscoveryStatus(QTmApplication*);
    void upnpApplicationStarted(QTmApplication *application);
    void upnpApplicationStopped(QTmApplication *application);
    void upnpApplicationStatusChanged(QTmApplication *application);
    void remoteServerDeviceAdded(QTmRemoteServer *remoteServer);
    void remoteServerDeviceRemoved(QTmRemoteServer *remoteServer);

    void keyboardToggle();
    void keyboardPressKey(int keySym);

private:
    QWidget     *getVncView(QWidget *upnpClientWidget);
    QWidget     *getUpnpView();
    QWidget     *getStatusView(QString label);
    QPushButton *getPushButton(QString icon, QSize size);


private:
    QTmApplication      *m_currentApplication;
    QTmClient           *m_TerminalMode;
    QVBoxLayout         *m_MainLayout;
    QTabWidget          *m_tabWidget;
    QAppScreen          *m_appScreen;
    Keyboard            *m_Keyboard;
    QPushButton         *m_RedButton;
    QPushButton         *m_AppButton;
    QPushButton         *m_ExitButton;
    QPushButton         *m_GreenButton;
    QPushButton         *m_UPnPAccess;
    QPushButton         *m_KeyboardAccess;
    QLabel              *m_statusLabel;
    int                  m_statusConnectIndex;
    int                  m_statusLoadingIndex;
    int                  m_statusBlockingIndex;
    int                  m_upnpIndex;
    QString              m_address;
    bool                 m_keyboardVisible;
    QTmClientProfile    *m_clientProfile;

    QList<QHostAddress>                   *m_interfaceList;
    QHash<QUrl, int>                      *m_connectionList;
    QHash<QWidget *, int>                 *m_widgetList;
    QHash<int, int>                       *m_tabList;
    QList<QTmRemoteServer*>               *m_remoteServerList;
    QMultiHash<int, int>                  *m_remoteServerIdToVncIdMap;

    QString m_clientId;
    QString m_friendlyName;
    QString m_manufacturer;
    QString m_modelName;
    QString m_modelNumber;
    QString m_mimetype;
    int     m_width;
    int     m_height;
    int     m_depth;
    QString m_bdAddr;
    bool    m_startConnection;
    bool    m_filtering;

    QTmGlobal::TmColorFormat  m_color;
    QSize                     m_size;
    QSize                     m_display;
    bool                      m_noRle;
    bool                      m_noInc;
    bool                      m_noScaling;

    void populateClientInformation();
    bool testTmClientProfile(QTmRemoteServer *remoteServerDevice, int profileID = 0);
    void setupStatusLabel();
    void setStatusText(QString statusText);

};

#endif // MYWIDGET_H
