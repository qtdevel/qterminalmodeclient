/***************************************************************************
**
** This file is part of qterminalmodeclient **
**
** Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).*
**
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
**  * Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**  * Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with ** the distribution.
**  * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the
**    names of its contributors may be used to endorse or promote products
**    derived from this software without specific ** prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
**************************************************************************/
#include "QAppScreen.h"

QAppScreen::QAppScreen(QSize size, QWidget *parent) : QWidget(parent) {
#ifdef QT_DEBUG_APPLICATION_SCREEN
    qDebug() << "QAppScreen::QAppScreen:" << "Create Application Screen";
#endif
    setLayout(new QVBoxLayout);
    m_deviceList = new QHash<int, QWidget *>();
    m_iconList   = new QList<QAppIcon *>();
    setMinimumSize(size);
}

void QAppScreen::updateApplicationList(QTmRemoteServer *remoteServer) {
    int id = remoteServer->remoteServerId();
#ifdef QT_DEBUG_APPLICATION_SCREEN
    qDebug() << "QAppScreen::updateApplicationList:" << "Update application list" << id;
#endif
    //Remove all old applications
    if (m_deviceList->contains(id))
        clearApplicationList(remoteServer);

    //Add all new applications
    QWidget *widget = new QWidget(this);
    widget->setLayout(new QHBoxLayout);
    layout()->addWidget(widget);

    QList<QTmApplication *> appList = remoteServer->applicationList();
    foreach (QTmApplication *application , appList) {
        QAppIcon *icon = new QAppIcon(application, this);
        widget->layout()->addWidget(icon);
        connect(icon, SIGNAL(clicked(QTmApplication*)), this, SIGNAL(launchApplication(QTmApplication*)));
        m_iconList->append(icon);
    }
    m_deviceList->insert(id, widget);
    update();
}


void QAppScreen::clearApplicationList(QTmRemoteServer *remoteServer) {
    int id = remoteServer->remoteServerId();
#ifdef QT_DEBUG_APPLICATION_SCREEN
    qDebug() << "QAppScreen::clearApplicationList:" << "Clear Application List" << id;
#endif
    if (!m_deviceList->contains(id))
    {
#ifdef QT_DEBUG_APPLICATION_SCREEN
        qDebug() << "QAppScreen::clearApplicationList: m_deviceList does not have id =" << id;
#endif
        return;
    }

    int iconListSize = m_iconList->size();
    for (int i=0; i<iconListSize; i++) {
        if (m_iconList->at(i)->application()->remoteServerId() == id) {
#ifdef QT_DEBUG_APPLICATION_SCREEN
            qDebug() << "QAppScreen::clearApplicationList:" << "... remove" << m_iconList->at(i)->application()->name();
#endif
            delete m_iconList->at(i);
            m_iconList->replace(i, NULL);
        }
    }
    m_iconList->removeAll(NULL);
    layout()->removeWidget(m_deviceList->value(id));
    layout()->update();
    m_deviceList->remove(id);
    update();
}
