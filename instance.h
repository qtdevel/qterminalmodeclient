#ifndef INSTANCE_H
#define INSTANCE_H

#include <QObject>

class QTerminalModeUPnPControlPoint;

class Instance : public QObject
{
    Q_OBJECT
public:
    explicit Instance(QObject *parent = 0);

signals:

public slots:
    void postInit();

private:
    QTerminalModeUPnPControlPoint *mControlPoint;
};

#endif // INSTANCE_H
