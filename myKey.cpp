/***************************************************************************
**
** This file is part of qterminalmodeclient **
**
** Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).*
**
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
**  * Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**  * Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with ** the distribution.
**  * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the
**    names of its contributors may be used to endorse or promote products
**    derived from this software without specific ** prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
**************************************************************************/

#include "myKey.h"
#include <QDebug>
#include <QPainter>
#include <QMouseEvent>

bool MyKey::toUpper = false;

MyKey::MyKey(QWidget * parent, QString text, int keyNr,
             KeyClass myClass, QString altText)
        : QWidget(parent)
{
    normalText = text;
    key = keyNr;
    myKeyClass = myClass;

    if (myKeyClass == keyCommon)
        alternateText = normalText.toUpper();
    else
        alternateText = altText;
    pressed = false;
}

void MyKey::setReleasedImg(QImage &img)
{
    releasedImage = img;
}

void MyKey::setPressedImg(QImage &img)
{
    pressedImage = img;
}

QImage& MyKey::releasedImg()
{
    return releasedImage;
}

MyKey::~MyKey()
{
}

KeyClass MyKey::keyClass()
{
    return myKeyClass;
}

void MyKey::changeCase()
{
    update();
}

void MyKey::shiftPressed()
{
    //keyCommon, keySymbol, keyFunction
    update();
}

QString MyKey::myText()
{
    if (myKeyClass == keyFunction || myKeyClass == keyNumber)
        return normalText;

    if (toUpper)
        return alternateText;
    else
        return normalText;

}

void MyKey::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPen pen(Qt::black, 1);
    painter.setPen(pen);
    painter.setRenderHint(QPainter::Antialiasing);
    QRect myBounds(0,0,size().width()-1, size().height()-1);

    if (this->myKey() == Qt::Key_Standby)
    {
        painter.drawImage(0, 0, releasedImage);
        painter.drawLine(8,8,size().width()-8, size().height()-8);
        painter.drawLine(8,size().height()-8, size().width()-8, 8);

    }
    else
    {
        if (pressed)
        {
            painter.drawImage(0, 0, pressedImage);
        }
        else
        {
            painter.drawImage(0, 0, releasedImage);
        }

        int fontSize = myKeyClass == keyFunction ? 13 : 18;
        painter.setFont(QFont("Bitstream Vera Sans", fontSize));
        painter.drawText(myBounds,Qt::AlignCenter, myText());
    }
}

void MyKey::mousePressEvent(QMouseEvent */*event*/)
{
    emit send(this);
    pressed = true;
    update();
}

void MyKey::mouseReleaseEvent(QMouseEvent */*event*/)
{
    pressed = false;
    update();
}

bool MyKey::isKeyPressed()
{
    return pressed;
}

int MyKey::myKey()
{
    QChar c;
    if(myKeyClass != keyFunction && toUpper == false)
    {
        if(normalText.length() > 0)
            c = normalText.at(0);
        else if(alternateText.length() > 0)
            c = alternateText.toLower().at(0);
        else
            return key;
        return c.unicode();
    }
    else
    if(myKeyClass != keyFunction && toUpper == true)
    {
        if(alternateText.length() > 0)
            c = alternateText.at(0);
        else if(normalText.length() > 0)
            c = normalText.toUpper().at(0);
        else
            return key;
        return c.unicode();
    }
    else
        return key;
}

