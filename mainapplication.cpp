/***************************************************************************
**
** This file is part of qterminalmodeclient **
**
** Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).*
**
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
**  * Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**  * Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with ** the distribution.
**  * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the
**    names of its contributors may be used to endorse or promote products
**    derived from this software without specific ** prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
**************************************************************************/

#include "mainapplication.h"
#include "QTmGlobal.h"
#include "QTmClient.h"

#include <QtGui>
#include "QTmApplication.h"

MainApplication::MainApplication(QWidget *parent) : QWidget(parent) {

    populateClientInformation();
    m_statusLabel = 0;
    m_Keyboard = 0;
    m_keyboardVisible  = false;
    m_MainLayout       = new QVBoxLayout(this);
    m_interfaceList    = new QList<QHostAddress>();
    m_connectionList   = new QHash<QUrl, int>();
    m_widgetList       = new QHash<QWidget *, int>();
    m_tabList          = new QHash<int, int>();
    m_remoteServerList = new QList<QTmRemoteServer *>;
    m_remoteServerIdToVncIdMap = new QMultiHash<int, int>();
    m_currentApplication = 0;
    m_clientProfile = new QTmClientProfile(m_clientId);

    QPalette palette;
    palette.setBrush(QPalette::Window, QColor(Qt::black));
    setPalette(palette);

    // very simple commandline parser
    QStringList list = QApplication::arguments();

    m_filtering = false;
    if (list.contains("--filter"))
        m_filtering = true;
    m_display = QSize(0,0);
    if (list.contains("--display")) {
        int idx = list.indexOf("--display");
        if (idx + 2 < list.size())
            m_display = QSize(list.at(idx+1).toInt(), list.at(idx+2).toInt());
    }
    m_size = QSize(640, 360);
    if (list.contains("--size")) {
        int idx = list.indexOf("--size");
        if (idx + 2 < list.size())
            m_size = QSize(list.at(idx+1).toInt(), list.at(idx+2).toInt());
    }

    if (list.contains("--rgb565"))
        m_color = QTmGlobal::RGB565;
    else if (list.contains("--rgb555"))
        m_color = QTmGlobal::RGB555;
    else if (list.contains("--rgb444"))
        m_color = QTmGlobal::RGB444;
    else if (list.contains("--rgb343"))
        m_color = QTmGlobal::RGB343;
    else
        m_color = QTmGlobal::RGB888;

    m_noRle     = !list.contains("--no-rle");
    m_noInc     = !list.contains("--no-inc");
    m_noScaling = !list.contains("--no-scaling");

    m_TerminalMode = new QTmClient(this);
    m_TerminalMode->upnpStartControlPoint();
    m_tabWidget    = new QTabWidget();
    m_statusConnectIndex = m_tabWidget->addTab(getStatusView("Connect your Phone"), "Phone Connect");
    m_statusLoadingIndex = m_tabWidget->addTab(getStatusView("Loading Application List"), "App Loading");
    if (m_filtering)
        m_statusBlockingIndex = m_tabWidget->addTab(getStatusView("Application not Allowed while Driving"), "App Blocking");
    QWidget *upnpWidget  = getUpnpView();
    m_upnpIndex          = m_tabWidget->addTab(upnpWidget, "UPnP Listings");
    m_tabWidget->setStyleSheet(
            "QTabWidget             { background: #00ff00 }"
            "QTabWidget::pane       { font-size: 24px; background: #000000; }"
            "QTabWidget::tab-bar    { alignment: center }"
            "QTabBar::tab:selected  { background: #000000; color: #ffffff; font-size: 12px }"
            "QTabBar::tab:!selected { background: #000000; color: #ffffff; font-size: 12px }"
            );
    m_MainLayout->addWidget(m_tabWidget);
    m_tabWidget->setCurrentIndex(m_statusConnectIndex);

    m_Keyboard = new Keyboard(this);
    connect(m_Keyboard, SIGNAL(deleteKeyboard()), this, SLOT(keyboardToggle()));
    connect(m_Keyboard, SIGNAL(keyPressed(int)), this, SLOT(keyboardPressKey(int)));
    m_Keyboard->setActivePos(100, 100);
    m_Keyboard->hide();

    if (!m_display.isNull()) {
        setMinimumSize(m_display);
        resize(m_display);
    }

    if (list.contains("--fullscreen")) {
        showFullScreen();
    }

    connect(m_TerminalMode, SIGNAL(vncClientConnected(int)),      this, SLOT(vncConnected(int)));
    connect(m_TerminalMode, SIGNAL(vncClientDisconnected(int)),   this, SLOT(vncDisconnected(int)));
    connect(m_TerminalMode, SIGNAL(vncClientResized(QSize, int)), this, SLOT(vncResized(QSize, int)));
    connect(m_TerminalMode, SIGNAL(vncClientContextInformation(QRect,int,QTmAppInfo*,QTmDisplayInfo*,int)),
            this,           SLOT  (vncClientContextInformation(QRect,int,QTmAppInfo*,QTmDisplayInfo*,int)));
    connect(m_TerminalMode, SIGNAL(remoteServerDeviceAdded(QTmRemoteServer*)),
            this,           SLOT  (remoteServerDeviceAdded(QTmRemoteServer*)));
    connect(m_TerminalMode, SIGNAL(remoteServerDeviceRemoved(QTmRemoteServer*)),
            this,           SLOT  (remoteServerDeviceRemoved(QTmRemoteServer*)));
    connect(m_TerminalMode, SIGNAL(networkDeviceDetected(QString, QHostAddress)), this, SLOT(deviceDetected(QString, QHostAddress)));
    connect(m_TerminalMode, SIGNAL(networkDeviceLost(QHostAddress)),              this, SLOT(deviceLost(QHostAddress)));

    //Check whether to use IPv6 interfaces or IPv4 interfaces
    if (list.contains("--ipv6"))
        m_TerminalMode->networkStartObserver(false, true);
    else
        m_TerminalMode->networkStartObserver(); //default
}

void MainApplication::setupStatusLabel()
{
    m_statusLabel = new QLabel(this);
    QPalette palette;

    //White text
    QBrush brush(QColor(Qt::gray));
    brush.setStyle(Qt::SolidPattern);

    //Set white text
    palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
    palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);

    //Black background
    QBrush brush1(QColor(0, 0, 0, 255));
    brush1.setStyle(Qt::SolidPattern);

    //Set black background
    palette.setBrush(QPalette::Active, QPalette::Window, brush1);
    palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);

    //Set palette
    m_statusLabel->setPalette(palette);

    //Set font
    QFont font;
    font.setPointSize(24);

    //Set label properties
    m_statusLabel->setFrameStyle( QFrame::Panel);
    m_statusLabel->setFont(font);
    m_statusLabel->setAutoFillBackground(true);
}

void MainApplication::setStatusText(QString statusText)
{
    m_statusLabel->setText(statusText);
}

void MainApplication::populateClientInformation()
{
    m_clientId = "TMClient1";
    m_friendlyName = "Terminal Mode Qt Demo Client";
    m_manufacturer = "Acme";
    m_modelName = "QtTerminalModeClient";
    m_modelNumber = "1";
    m_mimetype = "image/xpm";
    m_width = 40;
    m_height = 40;
    m_depth = 32;
    m_bdAddr = "0001020304050607";
    m_startConnection = false;
}

QWidget *MainApplication::getStatusView(QString label) {
    QWidget *view = new QWidget();
    view->setLayout(new QVBoxLayout);
    QWidget *status = new QLabel(QString("<h2><font color=\"white\">%1</font></h2>").arg(label), this);
    view->layout()->addWidget(status);
    view->layout()->setAlignment(status, Qt::AlignHCenter);
    view->show();
    view->setStyleSheet("background: #000000");
    return view;
}

QWidget *MainApplication::getUpnpView() {
    qDebug() << "MainApplication:" << "Create UPnP View";
    QWidget *view   = new QWidget();
    view->setLayout(new QHBoxLayout);
    m_appScreen = new QAppScreen(m_size, this);
    view->layout()->addWidget(m_appScreen);
    view->show();
    view->setStyleSheet("background: #000000");
    connect(m_appScreen,    SIGNAL(launchApplication(QTmApplication *)),
            this,           SLOT  (launchApplication(QTmApplication *)));
    return view;
}


QWidget *MainApplication::getVncView(QWidget *vncClientWidget) {
    qDebug() << "MainApplication:" << "Create VNC View";
    QWidget     *view   = new QWidget();
    QVBoxLayout *layout = new QVBoxLayout();
    QHBoxLayout *active = new QHBoxLayout();
    QHBoxLayout *button = new QHBoxLayout();
    active->addWidget(vncClientWidget);
    m_GreenButton    = getPushButton(":/Resource/green.png", QSize(64, 64));
    m_AppButton      = getPushButton(":/Resource/app.png", QSize(64, 64));
    m_RedButton      = getPushButton(":/Resource/red.png", QSize(64, 64));
    m_KeyboardAccess = getPushButton(":/Resource/keyboard_icon.png", QSize(64, 64));
    m_UPnPAccess     = getPushButton(":/Resource/back.png", QSize(64, 64));
    m_ExitButton     = getPushButton(":/Resource/application-exit.png", QSize(64, 64));

    button->addWidget(m_GreenButton);
    button->addWidget(m_AppButton);
    button->addWidget(m_RedButton);
    button->addWidget(m_KeyboardAccess);
    button->addWidget(m_UPnPAccess);
    button->addWidget(m_ExitButton);
    layout->addLayout(active, 1);
    layout->addLayout(button, 0);
    view->setLayout(layout);
    view->show();
    connect(m_GreenButton,    SIGNAL(clicked()), this, SLOT(clickGreen()));
    connect(m_AppButton,      SIGNAL(clicked()), this, SLOT(clickApp()));
    connect(m_RedButton,      SIGNAL(clicked()), this, SLOT(clickRed()));
    connect(m_KeyboardAccess, SIGNAL(clicked()), this, SLOT(keyboardToggle()));
    connect(m_UPnPAccess,     SIGNAL(clicked()), this, SLOT(clickUpnpView()));
    connect(m_ExitButton,     SIGNAL(clicked()), this, SLOT(clickAppExit()));
    view->setStyleSheet("background: #000000");
    return view;
}

QPushButton *MainApplication::getPushButton(QString icon, QSize size) {
    QPushButton *button = new QPushButton();
    button->setIcon(QIcon(icon));
    button->setFlat(true);
    button->setIconSize(size);
    button->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    button->setFocusPolicy(Qt::NoFocus);
    return button;
}


void MainApplication::vncResized(QSize size, int id) {
    qDebug() << "MainApplication:" << "(VNC Client" << id << "): Resized signal received" << size;
}

void MainApplication::vncConnected(int id) {
    qDebug() << "MainApplication:" << "VNC Client - Connected signal received";
    qDebug() << "MainApplication:" << "... from VNC client" << id;
    if (m_connectionList->key(id, QString()) == QString())
    {
        qDebug() << "MainApplication:" << "m_connectionList does not contain value 'id' =" << id;
        return;
    }
    m_tabWidget->setCurrentIndex(m_tabList->key(id));
    m_widgetList->key(id)->repaint();
    m_widgetList->key(id)->setFocus();
}

void MainApplication::vncDisconnected(int id) {

    qDebug() << "MainApplication:" << "(VNC Client" << id << "): Disconnected signal received";
    if (m_connectionList->key(id, QString()) == QString())
    {
        qDebug() << "MainApplication:" << "(VNC Client" << id << "): Disconnected signal not processed";
        return;
    }
    m_tabWidget->setCurrentIndex(m_upnpIndex);
    m_widgetList->remove(m_widgetList->key(id));
    m_tabWidget->removeTab(m_tabList->key(id));
    m_tabList->remove(m_tabList->key(id));
    m_connectionList->remove(m_connectionList->key(id));
    int remoteServerId = m_remoteServerIdToVncIdMap->key(id);
    m_remoteServerIdToVncIdMap->remove(remoteServerId, id);
    m_Keyboard->hide();
    m_tabWidget->repaint();
    m_appScreen->setFocus();
}

void MainApplication::vncClientContextInformation(QRect area, int appId, QTmAppInfo *appCategory, QTmDisplayInfo *conCategory, int id) {
    qDebug() << "MainApplication::vncContextInformation:" << "Received new context information" << id;
    qDebug() << "MainApplication::vncContextInformation:" << QString("- area") << area;
    qDebug() << "MainApplication::vncContextInformation:" << QString("- application: 0x%1").arg(appId, 1, 16);
    qDebug() << "MainApplication::vncContextInformation:" << QString("- category:    0x%1, trust level 0x%2").arg(appCategory->category(), 1, 16).arg(appCategory->trust(), 1, 16);
    qDebug() << "MainApplication::vncContextInformation:" << QString("- content:     0x%1, trust level 0x%2").arg(conCategory->category(), 1, 16).arg(conCategory->trust(), 1, 16);

    if (m_filtering) {
        int category = appCategory->category();
        if (category == QTmGlobal::AppNavigation   || category == QTmGlobal::AppMediaMusic ||
            category == QTmGlobal::AppUiHomeScreen || category == QTmGlobal::AppUiMenu ||
            category == QTmGlobal::AppPhoneCallLog || category == QTmGlobal::AppPhoneContactList ||
            category == QTmGlobal::AppPhone) {
            qDebug() << "MainApplication::vncClientContextInformation:" << "--> Application allowed";
            vncConnected(id);
            return;
        }
        else {
            qDebug() << "MainApplication::vncClientContextInformation:" << "--> Application not allowed";
            m_tabWidget->setCurrentIndex(m_statusBlockingIndex);
            m_TerminalMode->vncBlockFramebuffer(area, appId, 0x20, id);
        }
    }
}

void MainApplication::deviceDetected(QString interfaceName, QHostAddress interface) {
    qDebug() << "MainApplication:" << "Terminal Mode Device detected" << interface.toString();
    qDebug() << "MainApplication:" << "-Bearer Type: " << m_TerminalMode->networkBearerType(interfaceName);

    if (interface.isInSubnet(QHostAddress("10.0.0.1"), 8)) {
        qDebug() << "MainApplication:" << "... Skipping local LAN";
        return;
    }
    if (m_interfaceList->contains(interface))
        return;

    if (m_TerminalMode->upnpConnectControlPoint(interfaceName, interface)) {
        if (m_tabWidget->currentIndex() == m_statusConnectIndex) {
            m_tabWidget->setCurrentIndex(m_statusLoadingIndex);
        }
        m_interfaceList->append(interface);
    }
    else
        qDebug() << "MainApplication:" << "Could not start UPnP Control Point for" << interface.toString();
    repaint();
}

void MainApplication::deviceLost(QHostAddress interface) {
    qDebug() << "MainApplication:" << "Terminal Mode Device lost" << interface.toString();
    if (!m_interfaceList->contains(interface))
        return;

    m_TerminalMode->upnpDisconnectControlPoint(interface);
    m_interfaceList->removeAll(interface);
}

void MainApplication::upnpApplicationStarted(QTmApplication *application) {
    qDebug() << "MainApplication:" << "Application started" << application->name();

    if (m_connectionList->contains(application->url())) {
        qDebug() << "MainApplication:" << "... VNC Client already running at" << application->url();
        vncConnected(m_connectionList->value(application->url()));
        return;
    }
    int remoteServerId = application->remoteServerId();
    int id = m_TerminalMode->vncStartClient();
    m_remoteServerIdToVncIdMap->insert(remoteServerId, id);
    qDebug() << "MainApplication:" << "... start VNC Client" << id;
    m_connectionList->insert(application->url(), id);
    QWidget *widget = getVncView(m_TerminalMode->vncClientWidget(id));
    //insert m_TerminalMode->vncClientWidget to m_widgetList to setFocus (in vncConnected)
    m_widgetList->insert(widget, id);
    int tab = m_tabWidget->addTab(widget, QString("UI Layer %1 (VNC)").arg(id, 1, 10));
    m_tabList->insert(tab, id);
    //Configure VNC Client
    m_TerminalMode->vncSetColorFormat      (m_color,     id);
    m_TerminalMode->vncSetPreferredSize(m_size, m_noScaling, id);
    m_TerminalMode->vncSetRunLengthEncoding(m_noRle,     id);
    m_TerminalMode->vncSetIncrementalUpdate(m_noInc,     id);
    //Create VNC Tab View
    m_TerminalMode->vncConnectClient(application, id);
}

void MainApplication::upnpApplicationStopped(QTmApplication *application) {
    qDebug() << "MainApplication:" << "Application stopped" << application->name();
}

void MainApplication::upnpApplicationStatusChanged(QTmApplication *application) {
    QString appID = application->appId();
    QTmGlobal::TmStatus appStatusCode = application->status();
    qDebug() << "MainApplication: Application with ID" << appID << "has status" << appStatusCode;
    switch(appStatusCode)
    {
        case QTmGlobal::StatusNotrunning:
        {
            qDebug() << "MainApplication: Application with ID" << appID << "is not running.";
            //If application ID is that of current on-screen application, exit back to menu
            if(m_currentApplication)
            {
                if(!m_currentApplication->appId().compare(appID, Qt::CaseSensitive))
                {
                    m_currentApplication = 0;
                    clickAppExit();
                }
            }
        }break;
        case QTmGlobal::StatusForeground: qDebug() << "MainApplication: Application with ID" << appID << "is running (foreground)."; break;
        case QTmGlobal::StatusBackground: qDebug() << "MainApplication: Application with ID" << appID << "is running (background)."; break;
        case QTmGlobal::StatusUnknown:    qDebug() << "MainApplication: Application with ID" << appID << "is running with unknown status."; break;
    }
}


void MainApplication::keyboardToggle() {
    if (m_keyboardVisible) {
        qDebug() << "MainApplication:" << "Hide Virtual Keyboard";
        m_Keyboard->hide();
        m_keyboardVisible = false;
    }
    else {
        qDebug() << "MainApplication:" << "Start Virtual Keyboard";
        m_Keyboard->show();
        m_keyboardVisible = true;
    }
}
void MainApplication::keyboardPressKey(int keySym) {
    if (!m_tabList->contains(m_tabWidget->currentIndex()))
        return;
    m_TerminalMode->vncClickVirtualKey(keySym, m_tabList->value(m_tabWidget->currentIndex()));
}

void MainApplication::clickRed() {
    keyboardPressKey(QTmGlobal::XK_Device_Phone_end);
}

void MainApplication::clickGreen() {
    keyboardPressKey(QTmGlobal::XK_Device_Phone_call);
}

void MainApplication::clickApp() {
    keyboardPressKey(QTmGlobal::XK_Device_Application);
}

void MainApplication::clickAppExit() {
    if (!m_tabList->contains(m_tabWidget->currentIndex()))
        return;
    qDebug() << "MainApplication:" << "Exit the current application";
    qDebug() << "MainApplication:" << "... Switch to UPnP View";
    int id = m_tabList->value(m_tabWidget->currentIndex());
    qDebug() << "MainApplication:" << "... disconnect VNC Client" << id;
    m_TerminalMode->vncDisconnectClient(id);
    qDebug() << "MainApplication:" << "... stop VNC Client" << id;
    m_TerminalMode->vncStopClient(id);
    m_tabWidget->setCurrentIndex(m_upnpIndex);
    m_tabWidget->repaint();
    if (m_currentApplication == 0)
        return;
    qDebug() << "MainApplication:" << "... terminate Application";
    m_TerminalMode->upnpTerminateApplication(m_currentApplication);
    qDebug() << "MainApplication:" << "... done";
    m_currentApplication = 0;
}

void MainApplication::clickUpnpView() {
    qDebug() << "MainApplication:" << "Back from current application";
    qDebug() << "MainApplication:" << "... switch to UPnP View";
    m_tabWidget->setCurrentIndex(m_upnpIndex);
    m_tabWidget->repaint();
    //Force virtual keyboard to hide
    m_keyboardVisible = true;
    keyboardToggle();
}

void MainApplication::launchApplication(QTmApplication *application) {
    m_currentApplication = application;
    m_TerminalMode->upnpLaunchApplication(application);
}

void MainApplication::remoteServerDeviceAdded(QTmRemoteServer *remoteServer)
{
    if(!m_remoteServerList->contains(remoteServer))
    {
        //Init signal/slot mechanism
        connect(remoteServer, SIGNAL(applicationLaunched(QTmApplication*)),
                this,         SLOT(upnpApplicationStarted(QTmApplication*)));
        connect(remoteServer, SIGNAL(applicationTerminated(QTmApplication*)),
                this,         SLOT(upnpApplicationStopped(QTmApplication*)));
        connect(remoteServer, SIGNAL(applicationStatusChanged(QTmApplication*)),
                this,         SLOT(upnpApplicationStatusChanged(QTmApplication*)));
        connect(remoteServer, SIGNAL(applicationAdded(QTmApplication*)),
                this,         SLOT(updateApplicationDiscoveryStatus(QTmApplication*)));
        connect(remoteServer, SIGNAL(applicationListingChanged(QTmRemoteServer*)),
                m_appScreen,  SLOT(updateApplicationList(QTmRemoteServer*)));

        //Add remote server from list
        m_remoteServerList->append(remoteServer);
        qDebug() << "MainApplication: New Terminal Mode UPnP Server detected.";
        qDebug() << "MainApplication: - UDN:"  << remoteServer->UDN();
        qDebug() << "MainApplication: - id:"   << remoteServer->remoteServerId();
        qDebug() << "MainApplication: - Host:" << remoteServer->remoteServerUrl().host();

        int profileID = 0;
        if(testTmClientProfile(remoteServer, profileID)) {
            qDebug() <<  "MainApplication: testTmClientProfile(" << profileID << ") PASSED";
            if (m_tabWidget->currentIndex() == m_statusLoadingIndex) {
                m_tabWidget->setCurrentIndex(m_upnpIndex);
            }
            QString appListingFilter = QString("*");
            if(remoteServer->getApplicationList(appListingFilter, profileID))
                qDebug() <<  "MainApplication: getApplicationList( \"" << appListingFilter << "\"," << profileID << ") SUCCEED";
            else
                qDebug() <<  "MainApplication: getApplicationList( \"" << appListingFilter << "\"," << profileID << ") FAILED";
        } else
                qDebug() <<  "MainApplication: testTmClientProfile() FAILED";
    }
    else
    {
        qDebug() << "MainApplication: Terminal Mode UPnP Server with UDN:" << remoteServer->UDN() << " id:" << remoteServer->remoteServerId() << "already exists.";
    }
}

void MainApplication::remoteServerDeviceRemoved(QTmRemoteServer *remoteServer)
{
    //Clear applications list
    int remoteServerId = remoteServer->remoteServerId();
    m_appScreen->clearApplicationList(remoteServer);

    //Close all VNC clients associated with this remote server device
    int vncId;
    QHash<int, int>::iterator i = m_remoteServerIdToVncIdMap->find(remoteServerId);
    while (i != m_remoteServerIdToVncIdMap->end() && i.key() == remoteServerId) {
        vncId = i.value();
        m_TerminalMode->vncDisconnectClient(vncId);
        m_TerminalMode->vncStopClient(vncId);
        i = m_remoteServerIdToVncIdMap->find(remoteServerId);
    }
    //Remove all mappings for this remote server device
    m_remoteServerIdToVncIdMap->remove(remoteServerId);

    //Deinit signal/slot mechanism
    disconnect(remoteServer, SIGNAL(applicationLaunched(QTmApplication*)),
               this,         SLOT(upnpApplicationStarted(QTmApplication*)));
    disconnect(remoteServer, SIGNAL(applicationTerminated(QTmApplication*)),
               this,         SLOT(upnpApplicationStopped(QTmApplication*)));
    disconnect(remoteServer, SIGNAL(applicationStatusChanged(QTmApplication*)),
               this,         SLOT(upnpApplicationStatusChanged(QTmApplication*)));
    disconnect(remoteServer, SIGNAL(applicationAdded(QTmApplication*)),
               this,         SLOT(updateApplicationDiscoveryStatus(QTmApplication*)));
    disconnect(remoteServer, SIGNAL(applicationListingChanged(QTmRemoteServer*)),
               m_appScreen,  SLOT(updateApplicationList(QTmRemoteServer*)));

    //Remove remote server from list
    m_remoteServerList->removeAll(remoteServer);

    repaint();
}

bool MainApplication::testTmClientProfile(QTmRemoteServer *remoteServerDevice, int profileID)
{
    if(!remoteServerDevice)
    {
        qDebug() << "MainApplication: Cannot test TmClientProfile service as QTmRemoteServer object is NULL.";
    }

    bool testOK = false;

    qDebug() << "MainApplication: Testing TmClientProfile:1 service with profileID:" << profileID;
    m_clientProfile->setClientName(m_friendlyName);
    m_clientProfile->setManufacturerName(m_manufacturer);
    m_clientProfile->setModelName(m_modelName);
    m_clientProfile->setModelNumber(m_modelNumber);
    m_clientProfile->setIconMimeType(m_mimetype);
    m_clientProfile->setIconWidth(m_width);
    m_clientProfile->setIconHeight(m_height);
    m_clientProfile->setIconDepth(m_depth);
    m_clientProfile->setBdAddr(m_bdAddr);
    m_clientProfile->setStartBTConnection(m_startConnection);
    QTmClientRules rules;
    rules.setRule(0, "true");
    rules.setRule(1, "test");
    m_clientProfile->setClientRules(rules);

    //Test GetMaxNumProfiles
    qDebug() << "MainApplication: Max. Number of Client Profiles Allowed (-1 = FAIL):" << remoteServerDevice->getMaxNumProfiles();

    //Test SetClientProfile
    QTmClientProfile clientProfile1 = remoteServerDevice->setClientProfile(*m_clientProfile, profileID);
    if(clientProfile1.isNull())
        qDebug() << "MainApplication: testTmClientProfile(): SetClientProfile has returned a null profile";
    QString clientID1 = clientProfile1.clientId();
    QString friendlyName1 = clientProfile1.clientName();
    QString manufacturer1 = clientProfile1.manufacturerName();
    QString modelName1 = clientProfile1.modelName();
    QString modelNumber1 = clientProfile1.modelNumber();
    QString mimetype1 = clientProfile1.iconMimeType();
    int width1 = clientProfile1.iconWidth();
    int height1 = clientProfile1.iconHeight();
    int depth1 = clientProfile1.iconDepth();
    QString bdAddr1 = clientProfile1.bdAddr();
    bool startConnection1 = clientProfile1.startBTConnection();
    QString ruleValue0 = clientProfile1.clientRules().getRule(0);
    QString ruleValue1 = clientProfile1.clientRules().getRule(1);

    if(clientID1.compare(m_clientId) || friendlyName1.compare(m_friendlyName) || manufacturer1.compare(m_manufacturer) || modelName1.compare(m_modelName) || modelNumber1.compare(m_modelNumber)
        || mimetype1.compare(m_mimetype) || width1 != m_width || height1 != m_height || depth1 != m_depth || bdAddr1.compare(m_bdAddr) || startConnection1 != m_startConnection
        || ruleValue0.compare("true") || ruleValue1.compare("test"))
    {
        testOK = false;
        qDebug() << "MainApplication: testTmClientProfile(): SetClientProfile has failed the test";
    }
    else
    {
        testOK = true;
    }

    //Test GetClientProfile
    QTmClientProfile clientProfile2 = remoteServerDevice->getClientProfile(profileID);
    if(clientProfile2.isNull())
        qDebug() << "MainApplication: testTmClientProfile(): GetClientProfile has returned a null profile";
    clientID1 = clientProfile2.clientId();
    friendlyName1 = clientProfile2.clientName();
    manufacturer1 = clientProfile2.manufacturerName();
    modelName1 = clientProfile2.modelName();
    modelNumber1 = clientProfile2.modelNumber();
    mimetype1 = clientProfile2.iconMimeType();
    width1 = clientProfile2.iconWidth();
    height1 = clientProfile2.iconHeight();
    depth1 = clientProfile2.iconDepth();
    bdAddr1 = clientProfile2.bdAddr();
    startConnection1 = clientProfile2.startBTConnection();
    ruleValue0 = clientProfile2.clientRules().getRule(0);
    ruleValue1 = clientProfile2.clientRules().getRule(1);

    if(clientID1.compare(m_clientId) || friendlyName1.compare(m_friendlyName) || manufacturer1.compare(m_manufacturer) || modelName1.compare(m_modelName) || modelNumber1.compare(m_modelNumber)
        || mimetype1.compare(m_mimetype) || width1 != m_width || height1 != m_height || depth1 != m_depth || bdAddr1.compare(m_bdAddr) || startConnection1 != m_startConnection
        || ruleValue0.compare("true") || ruleValue1.compare("test"))
    {
        testOK = false;
        qDebug() << "MainApplication: testTmClientProfile(): SetClientProfile has failed the test";
    }
    else
    {
        testOK = true;
    }

    return testOK;
}

void MainApplication::updateApplicationDiscoveryStatus(QTmApplication *app)
{
    if(app && m_statusLabel)
        this->setStatusText(app->name());
}
