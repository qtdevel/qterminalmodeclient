#include "instance.h"

#include <QTimer>

#include <qterminalmodeupnpcontrolpoint.h>


Instance::Instance(QObject *parent) :
    QObject(parent)
{
    mControlPoint = new QTerminalModeUPnPControlPoint("usb0", this);

    QTimer::singleShot(0, this, SLOT(postInit()));
}

void Instance::postInit()
{
    mControlPoint->run();
}
