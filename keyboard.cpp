/***************************************************************************
**
** This file is part of qterminalmodeclient **
**
** Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).*
**
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
**  * Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**  * Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with ** the distribution.
**  * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the
**    names of its contributors may be used to endorse or promote products
**    derived from this software without specific ** prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
**************************************************************************/

#include "keyboard.h"

#include <QPainter>
#include <QMouseEvent>
#include <QEvent>
#include <QDebug>
#include <QKeyEvent>

Keyboard::Keyboard(QWidget *parent) : QWidget(parent) {
    m_drag              = false;
    m_activeImage       = QImage(":/Resource/keyboard_no_keys.png");
    m_currentActiveSize = m_activeImage.size();
    m_currentActivePos  = QPoint(5,130);
    setFixedSize(m_currentActiveSize);
    setupKeyboard();
    MyKey * key;
    foreach(key, m_keys)
        key->setVisible(true);
}

Keyboard::~Keyboard() {
    MyKey *key;
    foreach(key, m_keys)
        delete key;
    m_keys.clear();
}

QSize Keyboard::activeSize() {
    return m_currentActiveSize;
}

QPoint Keyboard::activePos() {
    return m_currentActivePos;
}

void Keyboard::setActivePos(int x, int y) {
    m_currentActivePos = QPoint(x, y);
    move(m_currentActivePos);
}

void Keyboard::paintEvent(QPaintEvent *) {
    QPainter painter(this);
    QImage img = m_activeImage;
    painter.drawImage(0, 0, img);
}

void Keyboard::mousePressEvent(QMouseEvent *event) {
    m_dragPoint = event->globalPos();
    m_drag = true;
}

void Keyboard::mouseReleaseEvent(QMouseEvent */*event*/) {
    m_drag = false;
    m_dragPoint = QPoint();
}

void Keyboard::mouseMoveEvent(QMouseEvent *event) {
    if (m_drag) {
        move(pos() + event->globalPos() - m_dragPoint);
        m_dragPoint = event->globalPos();
    }
}

void Keyboard::handleKeyPress(MyKey * key) {
    static bool wasUpper = false;

    QString text = key->myText();

    if (key->myKey() == Qt::Key_Standby)
    {
        emit deleteKeyboard();
        return;
    }

    if (key->myKey() == Qt::Key_Shift)
    {
        wasUpper = !wasUpper;
        MyKey::toUpper = wasUpper;
        emit changeCase();
        return;
    }

    emit keyPressed(key->myKey());

    if (wasUpper)
    {
        wasUpper = false;
        MyKey::toUpper = false;
        emit changeCase();
    }
    /*
    bool useShiftModifier = wasUpper;
    QKeyEvent * event = new QKeyEvent (QEvent::KeyPress, key->myKey(),
                                       useShiftModifier ? Qt::ShiftModifier : Qt::NoModifier,
                                       text, 1);
    QCoreApplication::postEvent ( parentWidget()->focusWidget(), event );

    event = new QKeyEvent (QEvent::KeyRelease, key->myKey(),
                           useShiftModifier ? Qt::ShiftModifier : Qt::NoModifier,
                           text, 1);
    QCoreApplication::postEvent ( parentWidget()->focusWidget(), event );
    */
}

void Keyboard::setupKeyboard() {
    //{keyCommon, keySymbol, keyFunction};

    MyKey *key;
    int lineBreak[5];
    int row = 0;

    // Row 1
    key = new MyKey(this, "0", Qt::Key_0, keyNumber);               m_keys << key;
    key = new MyKey(this, "1", Qt::Key_1, keyNumber);               m_keys << key;
    key = new MyKey(this, "2", Qt::Key_2, keyNumber);               m_keys << key;
    key = new MyKey(this, "3", Qt::Key_3, keyNumber);               m_keys << key;
    key = new MyKey(this, "4", Qt::Key_4, keyNumber);               m_keys << key;
    key = new MyKey(this, "5", Qt::Key_5, keyNumber);               m_keys << key;
    key = new MyKey(this, "6", Qt::Key_6, keyNumber);               m_keys << key;
    key = new MyKey(this, "7", Qt::Key_7, keyNumber);               m_keys << key;
    key = new MyKey(this, "8", Qt::Key_8, keyNumber);               m_keys << key;
    key = new MyKey(this, "9", Qt::Key_9, keyNumber);               m_keys << key;
    lineBreak[row++] = m_keys.indexOf(key) + 1;
    // Row 2
    key = new MyKey(this, "q", Qt::Key_Q, keyCommon);               m_keys << key;
    key = new MyKey(this, "w", Qt::Key_W, keyCommon);               m_keys << key;
    key = new MyKey(this, "e", Qt::Key_E, keyCommon);               m_keys << key;
    key = new MyKey(this, "r", Qt::Key_R, keyCommon);               m_keys << key;
    key = new MyKey(this, "t", Qt::Key_T, keyCommon);               m_keys << key;
    key = new MyKey(this, "y", Qt::Key_Y, keyCommon);               m_keys << key;
    key = new MyKey(this, "u", Qt::Key_U, keyCommon);               m_keys << key;
    key = new MyKey(this, "i", Qt::Key_I, keyCommon);               m_keys << key;
    key = new MyKey(this, "o", Qt::Key_O, keyCommon);               m_keys << key;
    key = new MyKey(this, "p", Qt::Key_P, keyCommon);               m_keys << key;
    key = new MyKey(this, "=", Qt::Key_Aring, keyCommon);           m_keys << key;
    lineBreak[row++] = m_keys.indexOf(key) + 1;
    // Row 3
    key = new MyKey(this, "a", Qt::Key_A, keyCommon);               m_keys << key;
    key = new MyKey(this, "s", Qt::Key_S, keyCommon);               m_keys << key;
    key = new MyKey(this, "d", Qt::Key_D, keyCommon);               m_keys << key;
    key = new MyKey(this, "f", Qt::Key_F, keyCommon);               m_keys << key;
    key = new MyKey(this, "g", Qt::Key_G, keyCommon);               m_keys << key;
    key = new MyKey(this, "h", Qt::Key_H, keyCommon);               m_keys << key;
    key = new MyKey(this, "j", Qt::Key_J, keyCommon);               m_keys << key;
    key = new MyKey(this, "k", Qt::Key_K, keyCommon);               m_keys << key;
    key = new MyKey(this, "l", Qt::Key_L, keyCommon);               m_keys << key;
    key = new MyKey(this, "*", Qt::Key_multiply, keySymbol, "~");   m_keys << key;
    key = new MyKey(this, "#", Qt::Key_ssharp, keySymbol, "?");     m_keys << key;
    lineBreak[row++] = m_keys.indexOf(key) + 1;
    // Row 4
    key = new MyKey(this, "@", Qt::Key_At, keyCommon);              m_keys << key;
    key = new MyKey(this, "z", Qt::Key_Z, keyCommon);               m_keys << key;
    key = new MyKey(this, "x", Qt::Key_X, keyCommon);               m_keys << key;
    key = new MyKey(this, "c", Qt::Key_C, keyCommon);               m_keys << key;
    key = new MyKey(this, "v", Qt::Key_V, keyCommon);               m_keys << key;
    key = new MyKey(this, "b", Qt::Key_B, keyCommon);               m_keys << key;
    key = new MyKey(this, "n", Qt::Key_N, keyCommon);               m_keys << key;
    key = new MyKey(this, "m", Qt::Key_M, keyCommon);               m_keys << key;
    key = new MyKey(this, ",", Qt::Key_Comma, keySymbol, ";");      m_keys << key;
    key = new MyKey(this, ".", Qt::Key_Period, keySymbol, ":");     m_keys << key;
    key = new MyKey(this, "Enter", Qt::Key_Enter, keyFunction);     m_keys << key;
    MyKey * keyEnter = key;
    lineBreak[row++] = m_keys.indexOf(key) + 1;
    // Row 5
    key = new MyKey(this, "Shift", Qt::Key_Shift, keyFunction);     m_keys << key;
    MyKey * keyShift = key;
    key = new MyKey(this, " ", Qt::Key_Space, keySymbol, " ");      m_keys << key;
    MyKey * keySpace = key;
    key = new MyKey(this, "/", Qt::Key_Slash, keySymbol, "\\");     m_keys << key;
    key = new MyKey(this, "-", Qt::Key_Minus, keySymbol, "_");      m_keys << key;
    key = new MyKey(this, "+", Qt::Key_Plus, keySymbol, "-");       m_keys << key;
    key = new MyKey(this, "Back", Qt::Key_Backspace, keyFunction);  m_keys << key;
    lineBreak[row++] = m_keys.indexOf(key) + 1;
    key = new MyKey(this, "Hide", Qt::Key_Standby, keyFunction);    m_keys << key;
    MyKey * keyHide = key;

    QImage imgLetter(":/Resource/keyboard_key_letter.png");
    QImage imgLetterPressed(":/Resource/keyboard_letter_press.png");
    QImage imgNnumber(":/Resource/keyboard_key_number.png");
    QImage imgNumberPressed(":/Resource/keyboard_number_press.png");
    QImage imgSpace(":/Resource/keyboard_key_space.png");
    QImage imgSpacePresed(":/Resource/keyboard_space_press.png");
    QImage imgEnterShift(":/Resource/keyboard_key_enter_shift.png");
    QImage imgEnterShiftPressed(":/Resource/keyboard_shift_enter_press.png");
    QImage imgClose(":/Resource/keyboard_key_close.png");

    // Some initial setup
    foreach (key, m_keys) {
        key->setVisible(false);
        connect(key, SIGNAL(send(MyKey*)), this, SLOT(handleKeyPress(MyKey*)));
        // Setup special keys
        if (key == keyEnter || key == keyShift)
        {
            key->setReleasedImg(imgEnterShift);
            key->setPressedImg(imgEnterShiftPressed);
        }
        else if (key == keySpace) 
        {
            key->setReleasedImg(imgSpace);
            key->setPressedImg(imgSpacePresed);
        }
        else if (key == keyHide)
        {
            key->setReleasedImg(imgClose);
        }
        else if (key->keyClass() == keyNumber)
        {
            key->setReleasedImg(imgNnumber);
            key->setPressedImg(imgNumberPressed);
        }
        else
        {
            key->setReleasedImg(imgLetter);
            key->setPressedImg(imgLetterPressed);
            connect(this, SIGNAL(changeCase()), key, SLOT(changeCase()));
        }
        key->setFixedSize(key->releasedImg().size());
    }

    int margin = 38;
    int spacing = 7;
    int numberSpacing = 6;
    int buttonWidth = m_keys[lineBreak[1]]->size().width();

    // Set layout of all buttons
    int iterator = 0;
    // Row 1: the numbers
    row = 0;
    int initialMargin = margin;
    int yPosition = initialMargin - 5;
    int xPosition = margin;
    for (iterator = 0; iterator < lineBreak[row]; iterator++)
    {
        m_keys[iterator]->move(xPosition, yPosition);
        xPosition += m_keys[iterator]->size().width() + numberSpacing;
    }

    // Row 2
    row++;
    xPosition = margin;
    yPosition += m_keys[0]->size().height() + spacing;
    for (iterator = lineBreak[row-1]; iterator < lineBreak[row]; iterator++)
    {
        m_keys[iterator]->move(xPosition, yPosition);
        xPosition += m_keys[iterator]->size().width() + spacing;
    }
    // Row 3
    row++;
    initialMargin = margin + (buttonWidth - spacing)/2 + spacing;
    xPosition = initialMargin;
    yPosition += buttonWidth + spacing;
    for (iterator = lineBreak[row-1]; iterator < lineBreak[row]; iterator++)
    {
        m_keys[iterator]->move(xPosition, yPosition);
        xPosition += m_keys[iterator]->size().width() + spacing;
    }
    // Row 4
    row++;
    initialMargin = margin;
    xPosition = initialMargin;
    yPosition += buttonWidth + spacing;
    for (iterator = lineBreak[row-1]; iterator < lineBreak[row]; iterator++)
    {
        m_keys[iterator]->move(xPosition, yPosition);
        xPosition += m_keys[iterator]->size().width() + spacing;
    }
    // Row 5
    row++;
    initialMargin = margin;
    xPosition = initialMargin;
    yPosition += buttonWidth + spacing;
    for (iterator = lineBreak[row-1]; iterator < lineBreak[row]; iterator++)
    {
        m_keys[iterator]->move(xPosition, yPosition);
        xPosition += m_keys[iterator]->size().width() + spacing;
    }
    // Hide key
    m_keys[m_keys.size()-1]->move(size().width()-m_keys[m_keys.size()-1]->size().width()-39, 35);

}
