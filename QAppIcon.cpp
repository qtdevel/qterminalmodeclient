/***************************************************************************
**
** This file is part of qterminalmodeclient **
**
** Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).*
**
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
**  * Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**  * Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with ** the distribution.
**  * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the
**    names of its contributors may be used to endorse or promote products
**    derived from this software without specific ** prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
**************************************************************************/

#include "QAppIcon.h"

QAppIcon::QAppIcon(QTmApplication *application, QWidget *parent) :
    QToolButton(parent) {
    m_application = application;
    qDebug() << "QAppIcon:" << "New App Icon" << application->name() << application->appId();

    setFocusPolicy(Qt::NoFocus);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setToolButtonStyle(Qt::ToolButtonIconOnly);
    setStyleSheet("QAppIcon { background-color:transparent; border: none; padding: 0px}");

    QList<QTmIcon *> iconList = application->iconList();
    if (!iconList.isEmpty()) {
        qDebug() << "QAppIcon:" << "... with icon";
        QTmIcon *icon = iconList.first();
        QPixmap pix(icon->url().toString());
        qDebug() << "QAppIcon:" << "... icon url" << icon->url().toString();
        setIcon(pix);
        setIconSize(QSize(128, 128));
    } else {
        qDebug() << "QAppIcon:" << "Add Name" << application->name() << application->url().toString();
        setText(application->name());
    }
    connect(this, SIGNAL(clicked()), this, SLOT(slotClick()));
}

QAppIcon::~QAppIcon() {
    //disconnect(this, SIGNAL(clicked()), this, SLOT(slotClick()));
    //delete m_labelIcon;
}

void QAppIcon::slotClick() {
    qDebug() << "QAppIcon:" << "Icon" << m_application->name() << "clicked";
    emit clicked(m_application);
}

