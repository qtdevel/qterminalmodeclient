# -------------------------------------------------
# Project qterminalmodeclient
# -------------------------------------------------
TARGET = qterminalmodeclient
TEMPLATE = app

debug: {
DEFINES += QT_DEBUG_APPLICATION_SCREEN
}

MOC_DIR = moc
CONFIG += debug
SOURCES += main.cpp \
    mainapplication.cpp \
    keyboard.cpp \
    myKey.cpp \
    QAppIcon.cpp \
    QAppScreen.cpp
HEADERS += mainapplication.h \
    keyboard.h \
    myKey.h \
    QAppIcon.h \
    QAppScreen.h
RESOURCES += resource.qrc
QT += xml \
    network \
    declarative
LIBS += -lQtTerminalMode
INCLUDEPATH += $$[QT_INSTALL_HEADERS]/QtTerminalMode
OTHER_FILES += ToDo.txt
